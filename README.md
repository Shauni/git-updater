# Git Updater

Git Updater is a Java library for dealing with git repository. Listing not up to date repository and allowing to update them.

## Installation

Actually, users should import that library by downloading it. We must provide another way to deal with it in the future.

## Usage

In order to use that library, users can use the different implementations that can be found in the `implementations` package.

All the different usage of the library are provided in the java documentation.

> Users also can build their own implementation by using interfaces provided in the `interfaces` package.

